package za.co.digitalplatoon.invoiceservice;

import static org.junit.Assert.assertTrue;

import za.co.digitalplatoon.invoiceservice.request.InvoiceRequest;
import za.co.digitalplatoon.invoiceservice.request.LineItemRequest;
import za.co.digitalplatoon.invoiceservice.response.BaseResponse;
import za.co.digitalplatoon.invoiceservice.service.InvoiceService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class InvoiceserviceApplicationTests {

  @Autowired
  private InvoiceService invoiceService;

  @Test
  void contextLoads() {
  }
  
  @Test
  void addInvoiceTest() {
    BaseResponse created = invoiceService.addInvoice(addInvoiceData());
    assertTrue(created != null);
  }

  private InvoiceRequest addInvoiceData() {
    InvoiceRequest request = new InvoiceRequest();
    request.setClient("Ajazahmed");
    request.setVatRate(5L);
    request.setInvoiceDate(LocalDate.parse("2020-06-04"));

    List<LineItemRequest> lineItemRequest = new ArrayList<LineItemRequest>();

    LineItemRequest lineRequest = new LineItemRequest();
    lineRequest.setDescription("Parle");
    lineRequest.setQuantity(4L);
    lineRequest.setUnitPrice(new BigDecimal(5.0));
    lineItemRequest.add(lineRequest);

    LineItemRequest lineRequest1 = new LineItemRequest();
    lineRequest1.setDescription("Monaco");
    lineRequest1.setQuantity(4L);
    lineRequest1.setUnitPrice(new BigDecimal(5.0));
    lineItemRequest.add(lineRequest1);

    request.setLineItems(lineItemRequest);
    return request;
  }

}
