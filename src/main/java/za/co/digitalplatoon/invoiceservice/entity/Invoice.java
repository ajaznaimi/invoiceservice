package za.co.digitalplatoon.invoiceservice.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table
public class Invoice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String client;
	
	@Column
	private Long vatRate;
	
	@Column
	private LocalDate invoiceDate;
	
	@OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL)
	private List<LineItem> lineItems;
	
}
