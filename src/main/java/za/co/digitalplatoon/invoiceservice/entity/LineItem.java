package za.co.digitalplatoon.invoiceservice.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table
public class LineItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
  private Long quantity;
	
	@Column
	private String description;
	
	@Column
	private BigDecimal unitPrice;
	
	@ManyToOne
	@JoinColumn(name="invoice_id", nullable = false)
	private Invoice invoice;

}
