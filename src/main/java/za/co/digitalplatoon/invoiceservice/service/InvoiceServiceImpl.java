package za.co.digitalplatoon.invoiceservice.service;

import za.co.digitalplatoon.invoiceservice.entity.Invoice;
import za.co.digitalplatoon.invoiceservice.entity.LineItem;
import za.co.digitalplatoon.invoiceservice.repository.InvoiceRepository;
import za.co.digitalplatoon.invoiceservice.request.InvoiceRequest;
import za.co.digitalplatoon.invoiceservice.response.BaseResponse;
import za.co.digitalplatoon.invoiceservice.response.InvoiceResponse;
import za.co.digitalplatoon.invoiceservice.response.InvoicesResponse;
import za.co.digitalplatoon.invoiceservice.response.LineItemResponse;
import za.co.digitalplatoon.invoiceservice.response.ResponseStatus;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class InvoiceServiceImpl implements InvoiceService {

  @Autowired
  private InvoiceRepository invoiceRepository;

  @Override
  public BaseResponse addInvoice(InvoiceRequest invoiceRequest) {
    BaseResponse response = new BaseResponse();
    Invoice invoice = new Invoice();
    invoice.setLineItems(new ArrayList<>());
    invoice.setClient(invoiceRequest.getClient());
    invoice.setVatRate(invoiceRequest.getVatRate());
    invoice.setInvoiceDate(invoiceRequest.getInvoiceDate());    

    if (!CollectionUtils.isEmpty(invoiceRequest.getLineItems())) {
      invoice.setLineItems(new ArrayList<>());
      invoiceRequest.getLineItems().forEach(item -> {
        LineItem lineItem = new LineItem();
        lineItem.setDescription(item.getDescription());
        lineItem.setQuantity(item.getQuantity());
        lineItem.setUnitPrice(item.getUnitPrice());
        lineItem.setInvoice(invoice);
        invoice.getLineItems().add(lineItem);
      });
    }
    invoiceRepository.save(invoice);
    response.setStatusCode(ResponseStatus.SUCCESS);
    response.setStatusMessage(BaseResponse.SUCCESS_STATUS_MESSAGE);
    return response;
  }

  @Override
  public InvoiceResponse viewInvoice(Long invoiceId) {
    InvoiceResponse invoiceResponse = new InvoiceResponse();

    Invoice invoice = null;

    try {
      invoice = invoiceRepository.getOne(invoiceId);
      setInvoiceData(invoice, invoiceResponse);
    } catch (EntityNotFoundException ex) {
      invoiceResponse.setStatusCode(ResponseStatus.FAILURE);
      invoiceResponse.setStatusMessage("No invoice is defined with invoiceId " + invoiceId);
      return invoiceResponse;
    }

    invoiceResponse.setStatusCode(ResponseStatus.SUCCESS);
    invoiceResponse.setStatusMessage(BaseResponse.SUCCESS_STATUS_MESSAGE);
    return invoiceResponse;
  }

  @Override
  public InvoicesResponse viewAllInvoices() {
    InvoicesResponse invoicesResponse = new InvoicesResponse();
    invoicesResponse.setInvoices(new ArrayList<>());

    invoiceRepository.findAll().forEach(invoice -> {
      InvoiceResponse invoiceResponse = new InvoiceResponse();
      setInvoiceData(invoice, invoiceResponse);
      invoicesResponse.getInvoices().add(invoiceResponse);
    });

    if (CollectionUtils.isEmpty(invoicesResponse.getInvoices())) {
      invoicesResponse.setStatusCode(ResponseStatus.FAILURE);
      invoicesResponse.setStatusMessage("No invoice is defined");
      return invoicesResponse;
    }
    invoicesResponse.setStatusCode(ResponseStatus.SUCCESS);
    invoicesResponse.setStatusMessage(BaseResponse.SUCCESS_STATUS_MESSAGE);
    return invoicesResponse;
  }

  private void setInvoiceData(Invoice invoice, InvoiceResponse invoiceResponse) {
    invoiceResponse.setInvoiceId(invoice.getId());
    invoiceResponse.setClient(invoice.getClient());
    invoiceResponse.setVatRate(invoice.getVatRate());
    invoiceResponse.setInvoiceDate(invoice.getInvoiceDate());
    invoiceResponse.setLineItems(new ArrayList<>());
    BigDecimal subTotal = new BigDecimal(0);

    for (LineItem lineItem : invoice.getLineItems()) {
      LineItemResponse lineItemResponse = new LineItemResponse();
      lineItemResponse.setDescription(lineItem.getDescription());
      lineItemResponse.setQuantity(lineItem.getQuantity());
      lineItemResponse.setUnitPrice(lineItem.getUnitPrice());
      BigDecimal lineItemTotal = lineItem.getUnitPrice().multiply(BigDecimal.valueOf(lineItem.getQuantity())).setScale(2, BigDecimal.ROUND_HALF_UP);
      lineItemResponse.setLineItemTotal(lineItemTotal);
      invoiceResponse.getLineItems().add(lineItemResponse);
      subTotal = subTotal.add(lineItemTotal);
    }
    invoiceResponse.setSubTotal(subTotal.setScale(2, BigDecimal.ROUND_HALF_UP));
    BigDecimal vat = invoiceResponse.getSubTotal().multiply(BigDecimal.valueOf(invoiceResponse.getVatRate())).divide(BigDecimal.valueOf(100)).setScale(2,
        BigDecimal.ROUND_HALF_UP);
    invoiceResponse.setTotal(invoiceResponse.getSubTotal().add(vat).setScale(2, BigDecimal.ROUND_HALF_UP));
  }

}
