package za.co.digitalplatoon.invoiceservice.service;

import za.co.digitalplatoon.invoiceservice.request.InvoiceRequest;
import za.co.digitalplatoon.invoiceservice.response.BaseResponse;
import za.co.digitalplatoon.invoiceservice.response.InvoiceResponse;
import za.co.digitalplatoon.invoiceservice.response.InvoicesResponse;

public interface InvoiceService {
  
  public BaseResponse addInvoice(InvoiceRequest invoiceRequest);
  
  public InvoiceResponse viewInvoice(Long invoiceId);
  
  public InvoicesResponse viewAllInvoices();
  
}
