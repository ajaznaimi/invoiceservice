package za.co.digitalplatoon.invoiceservice.response;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class LineItemResponse {
	
	private String description;
	private Long quantity;
	private BigDecimal unitPrice;
	private BigDecimal lineItemTotal;

}
