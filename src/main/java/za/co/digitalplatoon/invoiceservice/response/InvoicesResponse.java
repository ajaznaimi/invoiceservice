package za.co.digitalplatoon.invoiceservice.response;

import java.util.Collection;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class InvoicesResponse extends BaseResponse {
	private Collection<InvoiceResponse> invoices;
}
