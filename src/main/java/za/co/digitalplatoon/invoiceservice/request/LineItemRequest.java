package za.co.digitalplatoon.invoiceservice.request;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class LineItemRequest {
	
	private String description;
	private Long quantity;
	private BigDecimal unitPrice;

}
