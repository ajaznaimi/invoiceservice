package za.co.digitalplatoon.invoiceservice.controller;

import za.co.digitalplatoon.invoiceservice.request.InvoiceRequest;
import za.co.digitalplatoon.invoiceservice.response.BaseResponse;
import za.co.digitalplatoon.invoiceservice.response.InvoiceResponse;
import za.co.digitalplatoon.invoiceservice.response.InvoicesResponse;
import za.co.digitalplatoon.invoiceservice.service.InvoiceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("invoices")
public class InvoiceController {
	
	@Autowired
	private InvoiceService invoiceService;
	
	@PostMapping
  public ResponseEntity<BaseResponse> addInvoice(@RequestBody InvoiceRequest invoiceRequest) {
      return new ResponseEntity<BaseResponse>(invoiceService.addInvoice(invoiceRequest), HttpStatus.OK);
  }
	
	@GetMapping("/{invoiceId}")
	public ResponseEntity<InvoiceResponse> viewInvoice(@PathVariable Long invoiceId) {
	    return new ResponseEntity<InvoiceResponse>(invoiceService.viewInvoice(invoiceId), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<InvoicesResponse> viewAllInvoices() {
	    return new ResponseEntity<InvoicesResponse>(invoiceService.viewAllInvoices(), HttpStatus.OK);
	}

}
