DROP TABLE IF EXISTS line_item;
DROP TABLE IF EXISTS invoice;

CREATE TABLE invoice (id LONG AUTO_INCREMENT PRIMARY KEY, client VARCHAR(250), invoice_date VARCHAR(250), vat_rate LONG);
CREATE TABLE line_item (id LONG AUTO_INCREMENT PRIMARY KEY, description VARCHAR(250), quantity LONG, unit_price decimal(3,2), invoice_id LONG);

