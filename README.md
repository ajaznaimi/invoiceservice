# invoiceservice
This is a spring boot Restfull API. Here I have describe three method.

# setup 
First you have installed
- GIt
- Maven
- eclipse editor

# Description 
- There are three method here. 
- I have used embedded H2 database.

# 1) POST method : addInvoice
# API endpoint :
- http://localhost:8080/invoices
# Request json : 
{
	"client" : "X",
	"vatRate" : 5,
	"invoiceDate" : "2020-06-04",
	 "lineItems": [
      {
        "description": "Parle",
        "quantity" : 4,
        "unitPrice" : 5.0
      },
      {
        "description": "Monaco",
        "quantity" : 5,
        "unitPrice" : 4.0
      }
    ]
}

# 2) GET method : viewAllInvoices
# API endpoint :
- http://localhost:8080/invoices
# Response json : 
{
    "statusCode": "SUCCESS",
    "statusMessage": "Transaction processed successfully.",
    "invoices": [
        {
            "invoiceId": 1,
            "client": "X",
            "vatRate": 5,
            "invoiceDate": "2020-06-04",
            "subTotal": 40.00,
            "total": 42.00,
            "lineItems": [
                {
                    "description": "Parle",
                    "quantity": 4,
                    "unitPrice": 5.00,
                    "lineItemTotal": 20.00
                },
                {
                    "description": "Monaco",
                    "quantity": 5,
                    "unitPrice": 4.00,
                    "lineItemTotal": 20.00
                }
            ]
        }
    ]
}

# 3) GET method : viewInvoice
# API endpoint :
- http://localhost:8080/invoices/1
# Response json : 
{
    "statusCode": "SUCCESS",
    "statusMessage": "Transaction processed successfully.",
    "invoiceId": 1,
    "client": "X",
    "vatRate": 5,
    "invoiceDate": "2020-06-04",
    "subTotal": 40.00,
    "total": 42.00,
    "lineItems": [
        {
            "description": "Parle",
            "quantity": 4,
            "unitPrice": 5.00,
            "lineItemTotal": 20.00
        },
        {
            "description": "Monaco",
            "quantity": 5,
            "unitPrice": 4.00,
            "lineItemTotal": 20.00
        }
    ]
}